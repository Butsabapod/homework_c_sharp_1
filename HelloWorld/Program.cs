﻿using System;
using Butsaba.Location;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            User a = new User();
            a.age = 10;
            a.name = "Butsaba";
            a.show();
            Address Addr = new Address();
            Addr.homeNumber = "36";
            Console.WriteLine(Addr.getHomeNumber());

            Product PD = new Product();
            PD.price = 50;
            PD.name = "AA";
            Console.WriteLine(PD.getRecommendation(PD.price));
            PD.nameVegetable = "แตงกวา";
            Console.WriteLine(PD.getCategory(PD.nameVegetable));
            PD.levelname = "high";
            PD.pricelevel = 100;
            Console.WriteLine(PD.getReducedPrice(PD.levelname));
            PD.ProductName1 = "AA";
            PD.addToCart(PD.ProductName1);
            PD.ProductName1 = "BB";
            PD.addToCart(PD.ProductName1);
            PD.printAllProductInCart();
        }
    }
}

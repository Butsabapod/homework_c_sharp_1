﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld
{
    class Product
    {
        public int price;
        public String name;
        public String returnPrice;

        //public static ArrayList cart = new ArrayList(); 

        public String getRecommendation(int price1)
        {
            if (price1 > 100)
            {
                returnPrice = "แพง";
            }
            if (price1 >= 50 || price1 < 100) 
            {
                returnPrice = "ธรรมดา";
            }
            if (price1 < 50)
            {
                returnPrice = "ถูก";
            }

            return returnPrice;
        }

        public String nameVegetable;
        public String returnVegetable;
        public String nameV = "ผัก";
        public String getCategory(String nameVegetable1) 
        {
            if (nameVegetable1.Contains(nameV))
            //if (nameVegetable1.StartsWith("ผัก"))
            {
                returnVegetable = nameVegetable1;
                //return "ผัก";
            }
            else 
            {
                returnVegetable = "ไม่ผัก";
                //return "ไม่ผัก";
            }

            return returnVegetable;
        }

        public String levelname;
        public Double pricelevel;
        public Double returnPriceLevel;

        public Double getReducedPrice(String levelname1) 
        {
            switch (levelname1) 
            {
                case "high":
                    pricelevel = price - (price * 0.5);
                    returnPriceLevel = pricelevel;
                    break;

                case "medium":
                    pricelevel = price - (price * 0.3);
                    returnPriceLevel = pricelevel;
                    break;

                case "low":
                    pricelevel = price - (price * 0.1);
                    returnPriceLevel = pricelevel;
                    break;

                default:
                    Console.WriteLine("Default case");
                    break;
            }

            return returnPriceLevel;
        }

 
        List<String> cart = new List<String>();
        public String ProductName1;
        public String addToCart(String ProductName) 
        {
            cart.Add(ProductName);

            return "complete";
        }

        public void printAllProductInCart()
        {
            for (int i = 0; i < cart.Count; i++)
            {
                Console.WriteLine(cart[i]);
            }

            int j = 0;
            while (j < cart.Count)
            {
                Console.WriteLine(cart[j]);
                j++;
            }

            int[] numbers = { 4, 5, 6, 1, 2, 3, -2, -1, 0 };
            foreach (String k in cart)
            {
                Console.WriteLine(k);
            }
        }
    }
}
